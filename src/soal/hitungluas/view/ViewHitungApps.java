package soal.hitungluas.view;

import soal.hitungluas.logicrumus.Keliling;
import soal.hitungluas.logicrumus.Luas;
import soal.hitungluas.util.InputUtil;

import java.util.Scanner;

public class ViewHitungApps {

    private static InputUtil inputUtil = new InputUtil();
    private static Scanner input = new Scanner(System.in);
    private static Luas luas = new Luas();
    private static Keliling keliling = new Keliling();

    public static void viewApps(){
        while (true){
            showFirst();
            System.out.println("== MENU PILIHAN HITUNG RUMUS LUAS DAN KELILING ==");
            System.out.println("== 1. HITUNG LUAS LINGKARAN");
            System.out.println("== 2. HITUNG LUAS DAN KELILING SEGITIGA");
            System.out.println("== 3. HITUNG LUAS PERSEGI PANJANG");
            System.out.println("== 4. HITUNG KELILING PERSEGI PANJANG");

            inputUtil.hanyaGaris();
            System.out.print("== MASUKAN MENU PILIHAN (tekan X jika ingin keluar aplikasi) : ");
            var inputan = input.nextLine();
            if (inputan.equals("1")){
                luas.HitungLuasLingkaran();
            }else if (inputan.equals("2")){
                luas.HitungLuasdanKelilingSegitiga();
            } else if (inputan.equals("3")) {
                luas.HitungLuasPersegiPanjang();
            } else if (inputan.equals("4")) {
                keliling.HitungKelilingPersegiPanjang();
            } else if (inputan.equals("x")) {
                break;
            }else {
                inputUtil.hanyaGaris();
                System.out.println("MAAF INPUTAN TIDAK TERSEDIA");
            }

        }
    }

    private static void showFirst(){
        System.out.println("=== APLIKASI COMMAND LINE HITUNG RUMUS ALL VERSION RUMUS ==");
        inputUtil.hanyaGaris();
        System.out.println("== DEVELOP BY : GEETOOR.DAY ==");
        inputUtil.hanyaGaris();
    }

}
