package soal.hitungluas.logicrumus;

import soal.hitungluas.repo.Data;
import soal.hitungluas.util.InputUtil;

public class Keliling {
    Data data = new Data();
    InputUtil input = new InputUtil();

    public void HitungKelilingPersegiPanjang(){
        input.hanyaGaris();
        input.infoInput("PROGRAM HITUNG KELILING PERSEGI PANJANG");
        input.hanyaGaris();

        input.inputUser("Masukan Panjang");
        data.setPanjang(input.getData());

        input.inputUser("Masukan Lebar");
        data.setLebar(input.getData());

        data.setKeliling(2 * (data.getPanjang() + data.getLebar()));

        input.hanyaGaris();
        System.out.println("Hasil Keliling Persegi Panjang adalah : " + data.getKeliling());
        input.hanyaGaris();
    }

}
