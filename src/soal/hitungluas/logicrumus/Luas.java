package soal.hitungluas.logicrumus;

import soal.hitungluas.repo.Data;
import soal.hitungluas.util.InputUtil;

public class Luas {

    // Buat object dari class Data dan InputUtil
    Data data = new Data();
    InputUtil input = new InputUtil();

    // function untuk menghitung luas Lingkaran
    public void HitungLuasLingkaran(){

        input.hanyaGaris();
        input.infoInput("PROGRAM MENGHITUNG LUAS LINGKARAN");
        input.hanyaGaris();

        input.inputUser("Masukan Jari Jari Lingkaran");

        data.setJarijarilingkaran(input.getData());

        data.setLuas(data.getPhi() * data.getJarijarilingkaran());

        input.hanyaGaris();
        System.out.println("Hasil Dari program Hitung luas Lingkaran adalah : " + data.getLuas());
        input.hanyaGaris();
    }


    // function untuk menghitung luas dan keliling segitiga
    public void HitungLuasdanKelilingSegitiga(){

        input.hanyaGaris();
        System.out.println("PROGRAM MENGHITUNG LUAS DAN KELILING SEGITIGA");
        input.hanyaGaris();

        input.inputUser("Masukan Alas");
        data.setAlas(input.getData());

        input.inputUser("Masukan Tinggi");
        data.setTinggi(input.getData());

        data.setLuas(data.getLs() * data.getAlas() * data.getTinggi());

        input.hanyaGaris();

        System.out.println("Hasil Luas dan Keliling Segitiga Adalah : " + data.getLuas());

        input.hanyaGaris();
    }

    // function untuk menghitung luas persegi panjang
    public void HitungLuasPersegiPanjang(){

        input.hanyaGaris();
        input.infoInput("PROGRAM MENGHITUNG LUAS PERSEGI PANJANG");
        input.hanyaGaris();

        input.inputUser("Masukan Panjang");
        data.setPanjang(input.getData());

        input.inputUser("Masukan Lebar");
        data.setLebar(input.getData());

        data.setLuas(data.getPanjang() * data.getLebar());

        input.hanyaGaris();
        System.out.println("Hasil Luas Persegi Panjang Adalah : " + data.getLuas());
        input.hanyaGaris();
    }

}
