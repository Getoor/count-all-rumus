package soal.hitungluas;


import soal.hitungluas.test.UnitTest;
import soal.hitungluas.view.ViewHitungApps;

public class HitungApps {
    public static void main(String[] args) {
        // memanggil function static di package view tanpa mendeklarasikan objek (karena memakai keyword static)
        ViewHitungApps.viewApps();
    }
}
