package soal.hitungluas.repo;

import java.util.Scanner;

public class Data {
    private double phi = 3.14;
    private double ls = 0.5;
    private double luas;
    private double keliling;
    private double jarijarilingkaran;
    private double alas;
    private double tinggi;
    private double panjang;
    private double lebar;

    public double getPhi() {
        return phi;
    }

    public double getLs() {
        return ls;
    }


    public double getLuas() {
        return luas;
    }


    public double getKeliling() {
        return keliling;
    }

    public double getJarijarilingkaran() {
        return jarijarilingkaran;
    }

    public void setJarijarilingkaran(double jarijarilingkaran) {
        this.jarijarilingkaran = jarijarilingkaran;
    }

    public double getAlas() {
        return alas;
    }

    public void setAlas(double alas) {
        this.alas = alas;
    }

    public double getTinggi() {
        return tinggi;
    }

    public void setTinggi(double tinggi) {
        this.tinggi = tinggi;
    }

    public double getPanjang() {
        return panjang;
    }

    public void setPanjang(double panjang) {
        this.panjang = panjang;
    }

    public double getLebar() {
        return lebar;
    }

    public void setLebar(double lebar) {
        this.lebar = lebar;
    }

    public void setLuas(double luas) {
        this.luas = luas;
    }

    public void setKeliling(double keliling) {
        this.keliling = keliling;
    }
}
