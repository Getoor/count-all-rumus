package soal.hitungluas.util;

import java.util.Scanner;

public class InputUtil {
    private static Scanner scanner = new Scanner(System.in);

    private double data;

    public void inputUser(String info){
        System.out.print(info + " : ");
        this.data = scanner.nextDouble();
    }

    public void infoInput(String info){
        System.out.println("=== " + info + " ===");
    }

    public double getData(){
        return this.data;
    }

    public void hanyaGaris(){
        System.out.println("==============================================");
    }
}
