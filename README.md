## **APLIKASI BASIS COMMAND LINE UNTUK MENGHITUNG RUMUS**

CLONE : 
git clone [url_repository]

untuk penambahan rumus luas = masukan kode di dalam package logicrumus->Luas

untuk penambahan rumus keliling = masukan kode di dalam package logicrumus->Keliling

JIKA INGIN MENAMBAHKAN PERHITUNGAN RUMUS BARU CTH(rumus menghitung luas kubus) : sudah terdapat data attribute yang telah di siapkan di package repo->data.java , tinggal pakai logic rumusnya di package logicrumus->Rumusservice.java 

silahkan pull untuk menambahkan rumus terbaru yang belum tersedia, dan tambahkan data yang di butuhkan jika tidak ada di dalam package repo->data.java dan jangan lupa commit kembali yah... (usahakan tambah branch baru untuk commit penambahan fitur perhitungan rumus terbaru)

note : jika ada penambahan rumus baru... tampilkan di package view->ViewHitungApss dan panggil function yang telah di buat di dalam kondisi 

**IMAGE WHEN PROGRAM IS RUN :** 

![](src/image/image1.png)

### BY :

**AGUS KURNIAWAN**

_GEETOOR DAY_